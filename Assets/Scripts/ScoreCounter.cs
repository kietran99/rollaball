﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCounter : MonoBehaviour
{
    [SerializeField]
    private Text scoreText = null, winText = null;

    void Start()
    {
        scoreText.text = "Score: 0";
    }

    public void DisplayScore(int score)
    {
        scoreText.text = "Score: " + score.ToString();
    }

    public void DisplayWin()
    {
        winText.gameObject.SetActive(true);
    }
}
