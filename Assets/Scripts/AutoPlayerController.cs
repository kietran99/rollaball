﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoPlayerController : PlayerController
{
    private Queue positionQueue;

    private Vector3 nextLocation;

    private List<Vector3> extraPickup;

    private bool flag;

    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        positionQueue = new Queue();
        extraPickup = new List<Vector3>();
        flag = true;
        PickupSpawner.OnClick += OnPickupCreate;
    }

    void FixedUpdate()
    {
        if (positionQueue.Count <= 0) return;

        if (!flag) return;

        Vector3 movePos = (Vector3) positionQueue.Dequeue();
        Debug.Log("Dequeue: " + movePos);

        if (extraPickup.Contains(movePos))
        {
            extraPickup.Remove(movePos);
            
            if (extraPickup.Count == 0) 
            {
                flag = true;
                return;
            }

            movePos = (Vector3) positionQueue.Dequeue();
        }

        nextLocation = movePos;
        movePos -= myRigidbody.position;
        myRigidbody.AddForce(movePos * moveSpeed);
        flag = false;
    }

    void OnPickupCreate(Vector3 createPos)
    {
        positionQueue.Enqueue(createPos);
        nCollectibles++;    
    }

    void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("PickUp")) return;

        other.gameObject.SetActive(false);
        score++;
        scoreCounter.DisplayScore(score);

        if (other.gameObject.transform.position.x != nextLocation.x || other.gameObject.transform.position.z != nextLocation.z) 
        {
            Vector3 extra = other.gameObject.transform.position + new Vector3(0f, -.5f, 0f);
            extraPickup.Add(extra);
            return;
        }

        myRigidbody.velocity = Vector3.zero;
        myRigidbody.angularVelocity = Vector3.zero;
        flag = true;
        if (score >= nCollectibles) scoreCounter.DisplayWin();
    }
}
