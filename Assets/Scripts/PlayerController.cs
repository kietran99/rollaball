﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    protected float moveSpeed = 0f;

    [SerializeField]
    protected Rigidbody myRigidbody = null;

    [SerializeField]
    protected int nCollectibles = 0;

    [SerializeField]
    protected ScoreCounter scoreCounter = null;

    protected int score;

    void Start()
    {
        score = 0;
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        myRigidbody.AddForce(new Vector3(moveHorizontal, 0f, moveVertical) * moveSpeed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            score++;
            scoreCounter.DisplayScore(score);
            if (score >= nCollectibles) scoreCounter.DisplayWin();
        }
    }
}
