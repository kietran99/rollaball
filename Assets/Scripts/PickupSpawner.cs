﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PickupSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject pickup = null;

    [SerializeField]
    private LayerMask playground = 1;

    public static Action<Vector3> OnClick;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        Ray ray = Camera.main.ScreenPointToRay(mousePos);
        RaycastHit hit;

        if (!Input.GetMouseButtonDown(0)) return;

        if (!Physics.Raycast(ray, out hit, 100f, playground)) return;

        Vector3 spawnPos = hit.point + new Vector3(0f, .5f, 0f);
        Instantiate(pickup, spawnPos, Quaternion.identity);
        if (OnClick != null) OnClick(hit.point);
    }
}
